package ru.gooddocs.vladislav.bashim.data

/**
 * Created by vlad on 05.11.17.
 */
object SearchRepositoryProvider {

    fun provideSearchRepository(): SearchRepository {
        return SearchRepository(BashImApiService.create())
    }
}