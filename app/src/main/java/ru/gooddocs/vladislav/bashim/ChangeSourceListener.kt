package ru.gooddocs.vladislav.bashim

/**
 * Created by vlad on 05.11.17.
 */
interface ChangeSourceListener {
    fun sourceChanged(position: Int)
}